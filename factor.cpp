/*
 * Factor.cpp
 *
 *  Created on: Nov 20, 2010
 *      Author: tfranovic
 */
#include<cstdio>
#include<cstdlib>
#include "gmp.h"
#include<vector>
#include<math.h>
using namespace std;
int primes[]={2,3,5,7,11,13,17,19,23,29,
                31,37,41,43,47,53,59,61,67,71,
                73,79,83,89,97,101,103,107,109,113,
                127,131,137,139,149,151,157,163,167,173,
                179,181,191,193,197,199,211,223,227,229,
                233,239,241,251,257,263,269,271,277,281,
                283,293,307,311,313,317,331,337,347,349,
                353,359,367,373,379,383,389,397,401,409,
                419,421,431,433,439,443,449,457,461,463,
                467,479,487,491,499,503,509,521,523,541,
                547,557,563,569,571,577,587,593,599,601,
                607,613,617,619,631,641,643,647,653,659,
                661,673,677,683,691,701,709,719,727,733,
                739,743,751,757,761,769,773,787,797,809,
                811,821,823,827,829,839,853,857,859,863,
                877,881,883,887,907,911,919,929,937,941,
                947,953,967,971,977,983,991,997,1009,1013,
                1019,1021,1031,1033,1039,1049,1051,1061,1063,1069,
                1087,1091,1093,1097,1103,1109,1117,1123,1129,1151,
                1153,1163,1171,1181,1187,1193,1201,1213,1217,1223,
                1229,1231,1237,1249,1259,1277,1279,1283,1289,1291,
                1297,1301,1303,1307,1319,1321,1327,1361,1367,1373,
                1381,1399,1409,1423,1427,1429,1433,1439,1447,1451,
                1453,1459,1471,1481,1483,1487,1489,1493,1499,1511,
                1523,1531,1543,1549,1553,1559,1567,1571,1579,1583,
                1597,1601,1607,1609,1613,1619,1621,1627,1637,1657,
                1663,1667,1669,1693,1697,1699,1709,1721,1723,1733,
                1741,1747,1753,1759,1777,1783,1787,1789,1801,1811,
                1823,1831,1847,1861,1867,1871,1873,1877,1879,1889,
                1901,1907,1913,1931,1933,1949,1951,1973,1979,1987,
                1993,1997,1999,2003,2011,2017,2027,2029,2039,2053,
                2063,2069,2081,2083,2087,2089,2099,2111,2113,2129,
                2131,2137,2141,2143,2153,2161,2179,2203,2207,2213,
                2221,2237,2239,2243,2251,2267,2269,2273,2281,2287,
                2293,2297,2309,2311,2333,2339,2341,2347,2351,2357,
                2371,2377,2381,2383,2389,2393,2399,2411,2417,2423,
                2437,2441,2447,2459,2467,2473,2477,2503,2521,2531,
                2539,2543,2549,2551,2557,2579,2591,2593,2609,2617,
                2621,2633,2647,2657,2659,2663,2671,2677,2683,2687,
                2689,2693,2699,2707,2711,2713,2719,2729,2731,2741,
                2749,2753,2767,2777,2789,2791,2797,2801,2803,2819,
                2833,2837,2843,2851,2857,2861,2879,2887,2897,2903,
                2909,2917,2927,2939,2953,2957,2963,2969,2971,2999,
                3001,3011,3019,3023,3037,3041,3049,3061,3067,3079,
                3083,3089,3109,3119,3121,3137,3163,3167,3169,3181,
                3187,3191,3203,3209,3217,3221,3229,3251,3253,3257,
                3259,3271,3299,3301,3307,3313,3319,3323,3329,3331,
                3343,3347,3359,3361,3371,3373,3389,3391,3407,3413,
                3433,3449,3457,3461,3463,3467,3469,3491,3499,3511,
                3517,3527,3529,3533,3539,3541,3547,3557,3559,3571,
                3581,3583,3593,3607,3613,3617,3623,3631,3637,3643,
                3659,3671,3673,3677,3691,3697,3701,3709,3719,3727,
                3733,3739,3761,3767,3769,3779,3793,3797,3803,3821,
                3823,3833,3847,3851,3853,3863,3877,3881,3889,3907,
                3911,3917,3919,3923,3929,3931,3943,3947,3967,3989,
                4001,4003,4007,4013,4019,4021,4027,4049,4051,4057,
                4073,4079,4091,4093,4099,4111,4127,4129,4133,4139,
                4153,4157,4159,4177,4201,4211,4217,4219,4229,4231,
                4241,4243,4253,4259,4261,4271,4273,4283,4289,4297,
                4327,4337,4339,4349,4357,4363,4373,4391,4397,4409,
                4421,4423,4441,4447,4451,4457,4463,4481,4483,4493,
                4507,4513,4517,4519,4523,4547,4549,4561,4567,4583,
                4591,4597,4603,4621,4637,4639,4643,4649,4651,4657,
                4663,4673,4679,4691,4703,4721,4723,4729,4733,4751,
                4759,4783,4787,4789,4793,4799,4801,4813,4817,4831,
                4861,4871,4877,4889,4903,4909,4919,4931,4933,4937,
                4943,4951,4957,4967,4969,4973,4987,4993,4999,5003,
                5009,5011,5021,5023,5039,5051,5059,5077,5081,5087,
                5099,5101,5107,5113,5119,5147,5153,5167,5171,5179,
                5189,5197,5209};
vector<int> factorBase;
mpz_t xes[20800];
mpz_t fxes[20800];
vector<vector<bool> > facs;
vector<vector<bool> > v;
vector<vector<int> > candidates;
mpz_t factors[100];
int fI;
int fail;
int extra=5;
void factor(mpz_t,int);
void findFactorBase(mpz_t, int);
void buildSieveInterval(mpz_t, int);
void sieve(int);
void getV(mpz_t,int);
void gaussElim();
void checkCandidates(mpz_t);
void reset();
void print();
void division(mpz_t, unsigned int);
void quadSieve(mpz_t);
void init();
int main() {
        mpz_t n;
        mpz_init(n);
        init();
    while(mpz_inp_str(n,stdin,0)>0) {
        fI=0;
        fail=0;
        factor(n,100);
        print();
        printf("\n");
        fflush(stdout);
    }
    mpz_clear(n);
    return 0;
}
 
void factor(mpz_t n, int limit) {
        int divlim=mpz_sizeinbase(n,2);
        if(mpz_cmp_ui(n,1)==0)
                return;
        if(mpz_probab_prime_p(n,10)) {
                mpz_set(factors[fI++],n);
                return;
        }
        if(mpz_perfect_square_p(n)) {
                mpz_t div,r;
                mpz_init(div);
                mpz_init(r);
                mpz_sqrt(r,n);
                mpz_set(div,r);
                factor(r,limit);
                factor(div,limit);
                mpz_clear(r);
                mpz_clear(div);
                return;
        }
        if(divlim>limit) {
                return;
        }
        if(divlim>50)
                divlim=50*50;
        else
                divlim=divlim*divlim;
        //division(n,divlim);
        if(mpz_cmp_ui(n,1)!=0) {
                quadSieve(n);
        }
}
 
void findFactorBase(mpz_t n, int B) {
        mpz_t x;
        int i=0;
        mpz_init(x);
        factorBase.push_back(-1);
        while(primes[i]<B) {
                mpz_set_ui(x,primes[i]);
                if(primes[i]<3 || mpz_legendre(n,x)==1) {
                        factorBase.push_back(primes[i]);
                }
                i++;
        }
        mpz_clear(x);
}
 
void buildSieveInterval(mpz_t n, int M) {
        int idx=0;
        mpz_t t,t1;
        mpz_init(t);
        mpz_init(t1);
        mpz_sqrt(t,n);
        for(int i=-M;i<=M;i++) {
                if(i<0)
                        mpz_sub_ui(t1,t,abs(i));
                else
                        mpz_add_ui(t1,t,i);
                mpz_init_set(xes[idx],t1);
                mpz_pow_ui(t1,t1,2);
                mpz_sub(t1,t1,n);
                mpz_set(fxes[idx++],t1);
                facs.push_back(vector<bool>(factorBase.size()));
        }
        mpz_clear(t);
        mpz_clear(t1);
}
 
void sieve(int M) {
        for(int i=0;i<factorBase.size();i++) {
                if(i==0) {
                        for(int j=0;j<facs.size();j++) {
                                if(mpz_sgn(fxes[j])<0) {
                                        mpz_neg(fxes[j],fxes[j]);
                                        facs[j][i]=1;
                                }
                        }
                        continue;
                }
                for(int j=0;j<facs.size();j++) {
                        if(!mpz_divisible_ui_p(fxes[j],factorBase[i]))
                                continue;
                        for(int k=j;k<facs.size();k+=factorBase[i]) {
                                while(mpz_divisible_ui_p(fxes[k], factorBase[i])) {
                                        mpz_divexact_ui(fxes[k],fxes[k],factorBase[i]);
                                        facs[k][i]=facs[k][i]^1;
                                }
                        }
                }
        }
}
void getV(mpz_t n, int B) {
        int idx=0;
        for(int i=0;i<facs.size();i++) {
                if(mpz_cmp_ui(fxes[i],1)==0) {
                        v.push_back(facs[i]);
                        mpz_set(xes[idx],xes[i]);
                        mpz_set(fxes[idx],xes[i]);
                        mpz_pow_ui(fxes[idx],fxes[idx],2);
                        mpz_sub(fxes[idx],fxes[idx], n);
                        idx++;
                        if(v.size()>factorBase.size()+extra)
                                break;
                }
        }
}

void gaussElim() {
        int n=v.size();
        int oldM=v[0].size();
        for(int i=0;i<n;i++) {
                for(int j=0;j<n;j++) {
                        if(i==j)
                                v[i].push_back(1);
                        else
                                v[i].push_back(0);
                }
        }
        int m=v[0].size();
        int l=0;
        for(int i=0;i<min(oldM,n);i++) {
                if(v[l][i]!=1) {
                        for(int j=i+1;j<n;j++) {
                                if(v[j][i]==1) {
                                        vector<bool> t=v[j];
                                        v[j]=v[i];
                                        v[i]=t;
                                        break;
                                }
                        }
                }
                if(v[l][i]==1)
                        l++;
                else
                        continue;
                for(int j=l;j<n;j++) {
                        if(v[j][i]==1) {
                                for(int k=i;k<m;k++) {
                                        v[j][k]=v[j][k]^v[l-1][k];
                                }
                        }
                }
        }
        vector<int> k;
        for(int i=n-1;i>=0;i--) {
                for(int j=0;j<m;j++) {
                        if(v[i][j]) {
                                if(j<oldM)
                                        break;
                                else
                                        k.push_back(j-oldM);
                        }
                }
                candidates.push_back(k);
                k.clear();
        }
}
 
void checkCandidates(mpz_t n) {
        if(candidates.size()==0) {
                fail=1;
                return;
        }
        for(int i=0;i<candidates.size();i++) {
                mpz_t x,y;
                mpz_init_set_ui(x,1);
                mpz_init_set_ui(y,1);
                for(int j=0;j<candidates[i].size();j++) {
                        mpz_mul(x,x,xes[candidates[i][j]]);
                        mpz_mul(y,y,fxes[candidates[i][j]]);
                }
                mpz_mod(x,x,n);
                mpz_sqrt(y,y);
                mpz_mod(y,y,n);
                mpz_sub(x,x,y);
                mpz_gcd(x,x,n);
                if(mpz_cmpabs_ui(x, 1) && mpz_cmpabs(x, n)) {
                        mpz_set(y,x);
                        quadSieve(x);
                        mpz_divexact(y,n,y);
                        quadSieve(y);
                        mpz_clear(x);
                        mpz_clear(y);
                        return;
                }
                mpz_clear(x);
                mpz_clear(y);
        }
}
 
void reset() {
        factorBase.clear();
        facs.clear();
        v.clear();
        candidates.clear();
}
 
void print() {
        if(fail || fI==0) {
                fI=0;
                printf("fail\n");
                return;
        }
        while(fI>0) {
                gmp_printf("%Zd\n", factors[--fI]);
        }
}
unsigned int arr[] = {4,2,4,2,4,6,2,6};
void division(mpz_t n, unsigned int lim) {
    mpz_t q,r;
    unsigned long int f;
    int ai,divfail;
    mpz_init(q);
    mpz_init(r);
    f=mpz_scan1(n,0);
    mpz_div_2exp(n,n,f);
    while(f) {
        mpz_set_ui(factors[fI++],2);
        --f;
    }
    while(1) {
        mpz_tdiv_qr_ui(q,r,n,3);
        if(mpz_cmp_ui(r,0)!=0)
            break;
        mpz_set(n,q);
        mpz_set_ui(factors[fI++],3);
    }
    while(1) {
        mpz_tdiv_qr_ui(q,r,n,5);
        if(mpz_cmp_ui(r,0)!=0)
            break;
        mpz_set(n,q);
        mpz_set_ui(factors[fI++],5);
    }
    divfail=0;
    f=7;
    ai=0;
    while(mpz_cmp_ui(n,1)!=0) {
        mpz_tdiv_qr_ui(q,r,n,f);
        if(mpz_cmp_ui(r,0)!=0) {
            f+=arr[ai];
            if(mpz_cmp_ui(q,f)<0)
                break;
            ai=(ai+1)%7;
            divfail++;
            if(divfail>lim)
                break;
        }
        else {
            mpz_swap(n,q);
            mpz_set_ui(factors[fI++],f);
            fail=0;
        }
    }
    mpz_clear(q);
    mpz_clear(r);
}
 
void quadSieve(mpz_t n) {
        if(fail)
                return;
        if(mpz_cmp_ui(n,1)==0)
                        return;
        if(mpz_probab_prime_p(n,10)) {
                mpz_set(factors[fI++],n);
                return;
        }
        if(mpz_perfect_square_p(n)) {
                mpz_t r,div;
                mpz_init(r);
                mpz_sqrt(r,n);
                mpz_init_set(div,r);
                quadSieve(r);
                quadSieve(div);
                mpz_clear(r);
                mpz_clear(div);
                return;
        }
        reset();
        float logn = mpz_sizeinbase(n,2)/(1.442695);
        int B=(int)((double)pow(exp(sqrt(logn*log(logn))),0.5)+1);
        if(B>5200)
                B=5200;
        findFactorBase(n,B);
        unsigned int M=2*B;
        buildSieveInterval(n,M);
        sieve(M);
        getV(n,B);
        if (v.size()==0) {
                fail=1;
                return;
        }
        gaussElim();
        checkCandidates(n);
}
 
void init() {
        int i;
        for(i=0;i<100;i++)
                mpz_init2(factors[i],100);
}